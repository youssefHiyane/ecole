package school.cours.repoitories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import school.cours.entities.Student;

@RepositoryRestResource
public interface StudentRepository extends JpaRepository<Student, Long>{

}
